---
title: 使用 Rancher 來開 RKE2
subtitle: 管理 K8s 的工具
date: 2022-02-12
categories: ["share"]
tags: ["rancher", "rke2", "kubernetes"]
comments: true
---

## 前言
最後在幫公司研究地端的 K8s 方案，之前是用 k3s 和 RKE。而其中 RKE 居多，不過因為 k8s 快要把 docker drop 掉了就不能用 RKE 了。所以就想要再開始研究 k3s/RKE2，不過既然都是 rancher 家的工具，就不如直接從 rancher 這個 k8s cluster 管理工具研究吧。

## 這次實驗準備的環境
兩台機器分別充當 master 和 worker，建置的方法是透過 [https://gitlab.com/conan-chang/k8s-cluster-gce](https://gitlab.com/conan-chang/k8s-cluster-gce) 在 GCP 上建兩台 GCE instance 出來測試的。

## 步驟
### 準備 rancher
參考：https://rancher.com/quick-start  
master 裝 docker：https://rancher.com/docs/rancher/v2.5/en/installation/requirements/installing-docker/
```shell
curl https://releases.rancher.com/install-docker/20.10.sh | sh
```
(裝完 docker 之後第一個問題就是 rootless ，不過我看 rancher 好像都是需要 root 權限，所以後都是用 root 來操作 docker 囉)  
```shell
sudo docker run --privileged -d --restart=unless-stopped -p 80:80 -p 443:443 rancher/rancher
```
安裝過程會出現非常大量的 ERROR，不過因為最後打開瀏覽器是進得去的，我就沒有先花時間去研究了 (反正 rancher 家的東西問題是不少)  
打開登入頁面後會看到它會給你一個指令讓你去取得密碼，以下是我得到的範例 (`b5bf4c918a10` 是 container id):
```shell
root@rancher:/home/conanchang# docker logs b5bf4c918a10  2>&1 | grep "Bootstrap Password:"
2022/02/12 13:25:22 [INFO] Bootstrap Password: cg7zm47kqfk52b75rr6b2rj56mx4rmkkkk4ksg944xk566df4jqxt
```
接下來你可以用它自動生成的密碼或者自己重設一個都可以  

### 產生 cluster
接下來在跟據使用頁面操作就很簡單了，大概步驟就是:
1. 在頁面上 create cluster，這邊預設是 RKE，建議改成 RKE2/k3s。畢竟 RKE 只支援 docker runtime 的
2. 接下來頁面會提示你怎麼讓機器(master/worker) 加入這個 cluster，要小心的是連線要通。我因為沒有給機器外網 IP，使用 GCP IAP tunnel 連過去的時候因為是用本機:8443 -> rancher:443，導致自動產生的註冊指令是使用 8443 port，所以要手動修改一下才能讓註冊指令跑起來。
3. kube config 是可以下載到本機用的，不過懶得再開一個 tunnel 就沒有試了

### 後續要自動化的話
稍微查了一下 ，用 rancher cli 就可以做到了:
```shell
# 建 cluster
rancher cluster create new-cluster
# 生成註冊指令
ancher cluster add-node new-cluster --etcd --controlplane --worker
```
不過我沒有找到建立 RKE2/k3s 的寫法，因為當我設定版本的時候有被拒絕:
```shell
$ rancher cluster create --k8s-version v1.21.9+rke2r1 new-cluster
Available Kubernetes versions:
v1.21.9-rancher1-1
v1.22.6-rancher1-1
v1.18.20-rancher1-3
v1.19.16-rancher1-3
v1.20.15-rancher1-1
```
不過這就留到日後再研究吧  

### 總結
這次試玩的是 Rancher 這個幫忙管理 K8s cluster 的工具，感想是還蠻簡單的，中間完全沒有撞到甚麼雷。不過更多的一些細節像是 pod/server 的 CIDR，evicti 條件之類的就留到之後有需要再來研究。因為還有一些要付費的工具像 loft 也想試試看。
