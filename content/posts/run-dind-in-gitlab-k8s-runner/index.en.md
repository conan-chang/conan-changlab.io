---
title: Run "docker in docker" in gitlab k8s runner
subtitle: the most easily way
date: 2022-03-12
categories: ["share"]
tags: ["gitlab-runner", "kubernetes", "docker"]
comments: true
---

## Introduction
Running automation job in container can have a great isolation, make you confidence that you can always get the same result from the CI/CD pipeline. But the problem is that you may need a [docker daemon](https://docs.docker.com/get-started/overview/#the-docker-daemon) when you want to run docker command in container. 

GitLab provide [services](https://docs.gitlab.com/ee/ci/services/), so that you can follow the [Docker-in-Docker instruction](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-docker-in-docker) to run a docker daemon with your gitlab ci job. 

But how about the image layer cache? Imagine your have a package download/install layer in your Dockerfile, and it take 3 minutes to build. You definitely don't want to lose this layer cache, and you also don't want to have a different image hash code when you build a same image twice.  

This post will show you how to build a shared docker daemon for Kubernetes executor for GitLab Runner.  

## Docker daemon manifest
To run a docker daemon, you need the image [docker:dind](https://hub.docker.com/_/docker). There are some key point:
1. For persisting the data, I use a stateful set carry a PVC. You can find that I set the size of PVC is just 20GB, it is because I used GKE and the disk is expandable.
2. `DOCKER_TLS_CERTDIR` is set to "", it is the way disable TLS because I am lazy. You can check out the origin [entrypoint.sh](https://github.com/docker-library/docker/blob/aebfc851a03e56656392defc519af422cbdb15ca/dockerd-entrypoint.sh#L129-L131) to know what it means
3. `privileged` is true to run docker
4. `containerPort` is `2375` because I disable TLS, default port should be 2376.
5. Run a pod with `image: docker` to test this docker daemon
    ```shell
    $ kubectl run -ti --rm --restart=Never test-docker --image=docker sh
    If you don't see a command prompt, try pressing enter.
    / # DOCKER_HOST=tcp://dind:2375 docker info
    Client:
    Context:    default
    Debug Mode: false

    Server:
    Containers: 0
    Running: 0
    Paused: 0
    Stopped: 0
    Images: 0
    Server Version: 20.10.13
    Storage Driver: overlay2
    ...
    ```
```yaml
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: dind
spec:
  selector:
    matchLabels:
      app: dind
  serviceName: dind
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: dind
    spec:
      containers:
      - name: dind
        image: docker:dind
        # # https://github.com/docker-library/docker/blob/aebfc851a03e56656392defc519af422cbdb15ca/dockerd-entrypoint.sh#L129-L131
        # # I think you can do something like:
        # command: ["dockerd", "--host", "tcp://0.0.0.0:2375"]
        env:
        # but defind the DOCKER_TLS_CERTDIR to "" can just do fine
        - name: DOCKER_TLS_CERTDIR
          value: ""
        ports:
        - containerPort: 2375
          name: docker-socket
        startupProbe:
          tcpSocket:
            port: 2375
          initialDelaySeconds: 5
          periodSeconds: 2
          successThreshold: 1
          # 120 secound just be realy enough
          failureThreshold: 60
        readinessProbe:
          tcpSocket:
            port: 2375
          initialDelaySeconds: 1
          periodSeconds: 30
          failureThreshold: 1
        livenessProbe:
          tcpSocket:
            port: 2375
          initialDelaySeconds: 1
          periodSeconds: 30
          failureThreshold: 2
        volumeMounts:
        - name: docker
          mountPath: /var/lib/docker
        resources:
          limits:
            memory: "256Mi"
            cpu: "1500m"
          requests:
            cpu: "32m"
        # dind required privileged
        securityContext:
          privileged: true
  volumeClaimTemplates:
  - metadata:
      name: docker
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 20Gi
---
apiVersion: v1
kind: Service
metadata:
  name: dind
spec:
  selector:
    app: dind
  ports:
  - port: 2375
    targetPort: 2375
```

## Runner helm chart values.yaml
Here is the helm values.yaml
```yaml
# helm upgrade --install gitlab-runner-devops -f values.yaml gitlab/gitlab-runner --version=0.34.2
gitlabUrl: https://gitlab.com/
runnerRegistrationToken: you-should-have-a-different-token
# fyr
concurrent: 4
install: true
rbac:
  create: true
checkInterval: 0
runners:
  tags: "k8s-runner"
  env:
    # overrides default daemon socket to connect to.
    # dind is the k8s service of docker daemon
    DOCKER_HOST: tcp://dind:2375
  # have the permission the run docker
  privileged: true
  locked: false
#   # fyr
#   builds:
#     cpuLimit: 1000m
#     memoryLimit: 1024Mi
#     cpuRequests: 200m
#     memoryRequests: 512Mi
  cache:
    cacheType: gcs
    gcsBucketName: just-want-to-remind-that-to-set-up-shared-cache-for-k8s-executor
    secretName: just-want-to-remind-that-to-set-up-shared-cache-for-k8s-executor
    cacheShared: true
  config: |
    [[runners]]
      [runners.kubernetes]
        # avoid dockerhub rate limit
        helper_image = "registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:x86_64-${CI_RUNNER_REVISION}"
        pull_policy = "if-not-present"
        poll_timeout = 1800
        # # fyr, if you need some scheduling/scaling strategy
        # [runners.kubernetes.node_selector]
        #   purpose = "gitlab-runner"
        # [runners.kubernetes.node_tolerations]
        #   "node-purpose-ns=not-gitlab-runner" = "NoSchedule"
        #   "node-purpose-ne=gitlab-runner" = "NoExecute"
```

Here is the `.gitlab-ci.yml` I used to test if this runner can run docker command:
```yaml
default:
  image: docker
  tags: ["k8s-runner"]

stages:
  - test
    
job:
  stage: test
  script:
    - docker info
    - docker ps
    - docker images
```