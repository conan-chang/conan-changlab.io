---
title: 在 GitLab k8s runner 裡使用 docker
subtitle: 最簡單的方法
date: 2022-03-12
categories: ["share"]
tags: ["gitlab-runner", "kubernetes", "docker"]
comments: true
---

## 簡介
在 docker container 裡面運行自動任務可以得到很好的隔離性，你不用擔心會不會被上次執行留下來的東西搞砸了下次的流程。或者更誇張的是 pipeline 早就應該要壞掉了，但是依靠著一個已經髒掉的 CI/CD 環境才徥以運作，然後害大家不敢對那個環境做出任何改動。阿離題了，回來今天要分享的是 GitLab Runner 使用 k8s executor 可以解決上述問題，但是在使用 docker 上就會很麻煩了。

因為要使用 docker 就需要一個 [docker daemon](https://docs.docker.com/get-started/overview/#the-docker-daemon)。GitLab 本身有提供 [services](https://docs.gitlab.com/ee/ci/services/) 這個功能，讓你可以外掛一個 docker daemon，詳見它們提供的 [Docker-in-Docker 解法](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-docker-in-docker)。

這樣用是能用了，但是你的 image layer cache 怎麼辦？想像一下那些在 Dockerfile 最底層每次都要花個 5 分鐘來下載/安裝的 packer layer，每次都啟個新的 docker daemon 就代表說都沒有 cache 可以用了。而且你按兩次 pipeline 會得到兩個不同 hash code 的 image，推到 container registry 就代表花兩倍的儲存空間，既複雜又花錢。  

這篇文章會展示一下我是怎麼讓我的 GitLab Kubernetes executor runner 可以使用一個共用的 docker

## Docker daemon manifest
要執行 docker daemon，可以利用官方的 [docker:dind](https://hub.docker.com/_/docker) image. 在 K8s 裡面運作有以下有幾個重點
1. 建 PVC 來儲存資料。下面我是用很小的 20GB，那是因為我是建在 GKE 裡，gce-pd 可以做擴展。
2. `DOCKER_TLS_CERTDIR` 設了 "", 這是因為我懶得弄 TLS，直接把它關了。你可以看原本這個 image 的 [entrypoint.sh](https://github.com/docker-library/docker/blob/aebfc851a03e56656392defc519af422cbdb15ca/dockerd-entrypoint.sh#L129-L131) 來了解這個設定是怎麼來的。
3. `privileged` 要開給 docker 用。
4. `containerPort` 設 `2375` 因為這是 non-TLS 的 port, 預設的 port 應該會是 2376。
5. 如果你想測試的話可以像下面這麼做。
    ```shell
    $ kubectl run -ti --rm --restart=Never test-docker --image=docker sh
    If you don't see a command prompt, try pressing enter.
    / # DOCKER_HOST=tcp://dind:2375 docker info
    Client:
    Context:    default
    Debug Mode: false

    Server:
    Containers: 0
    Running: 0
    Paused: 0
    Stopped: 0
    Images: 0
    Server Version: 20.10.13
    Storage Driver: overlay2
    ...
    ```
```yaml
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: dind
spec:
  selector:
    matchLabels:
      app: dind
  serviceName: dind
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: dind
    spec:
      containers:
      - name: dind
        image: docker:dind
        # # https://github.com/docker-library/docker/blob/aebfc851a03e56656392defc519af422cbdb15ca/dockerd-entrypoint.sh#L129-L131
        # # I think you can do something like:
        # command: ["dockerd", "--host", "tcp://0.0.0.0:2375"]
        env:
        # but defind the DOCKER_TLS_CERTDIR to "" can just do fine
        - name: DOCKER_TLS_CERTDIR
          value: ""
        ports:
        - containerPort: 2375
          name: docker-socket
        startupProbe:
          tcpSocket:
            port: 2375
          initialDelaySeconds: 5
          periodSeconds: 2
          successThreshold: 1
          # 120 secound just be realy enough
          failureThreshold: 60
        readinessProbe:
          tcpSocket:
            port: 2375
          initialDelaySeconds: 1
          periodSeconds: 30
          failureThreshold: 1
        livenessProbe:
          tcpSocket:
            port: 2375
          initialDelaySeconds: 1
          periodSeconds: 30
          failureThreshold: 2
        volumeMounts:
        - name: docker
          mountPath: /var/lib/docker
        resources:
          limits:
            memory: "256Mi"
            cpu: "1500m"
          requests:
            cpu: "32m"
        # dind required privileged
        securityContext:
          privileged: true
  volumeClaimTemplates:
  - metadata:
      name: docker
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 20Gi
---
apiVersion: v1
kind: Service
metadata:
  name: dind
spec:
  selector:
    app: dind
  ports:
  - port: 2375
    targetPort: 2375
```

## Runner 的 helm chart values.yaml
這是沒有省略甚麼欄位，是可以直接拿去改點資料就能用的程度：
```yaml
# helm upgrade --install gitlab-runner-devops -f values.yaml gitlab/gitlab-runner --version=0.34.2
gitlabUrl: https://gitlab.com/
runnerRegistrationToken: you-should-have-a-different-token
# fyr
concurrent: 4
install: true
rbac:
  create: true
checkInterval: 0
runners:
  tags: "k8s-runner"
  env:
    # overrides default daemon socket to connect to.
    # dind is the k8s service of docker daemon
    DOCKER_HOST: tcp://dind:2375
  # have the permission the run docker
  privileged: true
  locked: false
#   # fyr
#   builds:
#     cpuLimit: 1000m
#     memoryLimit: 1024Mi
#     cpuRequests: 200m
#     memoryRequests: 512Mi
  cache:
    cacheType: gcs
    gcsBucketName: just-want-to-remind-that-to-set-up-shared-cache-for-k8s-executor
    secretName: just-want-to-remind-that-to-set-up-shared-cache-for-k8s-executor
    cacheShared: true
  config: |
    [[runners]]
      [runners.kubernetes]
        # avoid dockerhub rate limit
        helper_image = "registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:x86_64-${CI_RUNNER_REVISION}"
        pull_policy = "if-not-present"
        poll_timeout = 1800
        # # fyr, if you need some scheduling/scaling strategy
        # [runners.kubernetes.node_selector]
        #   purpose = "gitlab-runner"
        # [runners.kubernetes.node_tolerations]
        #   "node-purpose-ns=not-gitlab-runner" = "NoSchedule"
        #   "node-purpose-ne=gitlab-runner" = "NoExecute"
```
裝完之後你的 GitLab CI job 就可以直接用 docker 了，這邊是個示範的 `.gitlab-ci.yml`：
```yaml
default:
  image: docker
  tags: ["k8s-runner"]

stages:
  - test
    
job:
  stage: test
  script:
    - docker info
    - docker ps
    - docker images
```