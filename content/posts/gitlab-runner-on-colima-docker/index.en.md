---
title: Install gitlab runner on colima docker
subtitle: with colima docker executor
date: 2022-03-05
categories: ["learning"]
tags: ["gitlab-runner", "colima", "docker"]
comments: true
---

## Introduce
If you want to run gitlab ci job/pipeline on your laptop, using docker should be a good choice because you probably don't want to mess up your working environment.

### colima
Using docker is the most easily way to install gitlab runner. The reason why I want to write the post is because I am using [colima](https://github.com/abiosoft/colima) to run docker, if I want to bind docker socket in GitLab pipeline job with colima, I have to care about the volume mounted.  
There is the command how I start colima, you can see I mount my ~/Desktop in colima VM, it is because I want to modify the config file of gitlab runner driectly.
```shell
colima start --mount /Users/changconan/Desktop:w
```

## Install
The first thing is to find out the docker socket in colima VM
```shell
❯ colima ssh
conanchang@lima-colima:/Users/conanchang$ docker context ls
NAME        DESCRIPTION                               DOCKER ENDPOINT               KUBERNETES ENDPOINT   ORCHESTRATOR
default *   Current DOCKER_HOST based configuration   unix:///var/run/docker.sock
```
It is the default path `/var/run/docker.sock`, I will mount it in gitlab job's container later.

```shell
docker run -d --name gitlab-runner --restart always \
  -v ~/Desktop/.gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock:rw \
  gitlab/gitlab-runner:v14.8.2
```
The `config` mount is my `laptop -> colima VM -> gitlab-runner container`. The docker socket mount is `colima VM -> gitlab-runner job's container`.

### Register
Register a runner, here is my command:
```shell
docker exec gitlab-runner gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "go-to-your-group-or-project-setting-cicd-runner-to-find-this-value" \
  --registration-token "go-to-your-group-or-project-setting-cicd-runner-to-find-this-value" \
  --description "docker-runner" \
  --tag-list "dockerd" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock
```
Now I can check registered record on my GitLab, and also check the config file direct on my ~/Desktop path:
```shell
❯ cat ~/Desktop/.gitlab-runner/config/config.toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "docker-runner"
  url = "https://gitlab.com/"
  token = "you-should-have-a-different-token"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```
The good thing you install gitlab runner with docker is that you don't have to do something like `gitlab-runner install` or `gitlab-runner start`, it is managed by docker.

### Test the runner
Here is the `.gitlab-ci.yml` I used to test if this runner can run docker command:
```yaml
default:
  image: docker
  tags: ["dockerd"]

stages:
  - test
    
job:
  stage: test
  script:
    - docker info
    - docker ps
    - docker images
```

## Conclusion
The best way to install gitlab runner is to use docker. All the thing is managed by docker, and even the pipeline cache is stored in docker volume.
