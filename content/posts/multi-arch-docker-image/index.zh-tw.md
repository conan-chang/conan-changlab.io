---
title: 使用 Docker buildx 架構出多平台 image
subtitle: 當身邊出現 amd64 以外的架構的時候，容器技術要怎麼支援其他平台
date: 2021-09-19
categories: ["share"]
tags: ["docker", "arch"]
comments: true
page:
    theme: "full"
---

## 事情就這麼發生了
最近有客戶在 workshop 中反映他使用的 M1 MacBook 沒辦法使用我們提供的 container image，於是我們就進入了支援多晶片架構的時代了。過往使用預設 amd64 架構走天涯的體驗回不去了。這篇文章的內容是建基於 [Docker 官方文件](https://docs.docker.com/buildx/working-with-buildx/)之上加入更多的解釋，所以也建議先讀/後讀一下原文。

## 這篇文章的目標
作為一個簡單的例子，就建構出具備 kubectl 這個工具的 container image，同時讓這個 image 支援多平台架構吧。  
![what-multi-arch-on-dockerhub-look-like](./multi-arch-on-dockerhub.png)
當使用者 docker pull 這個 image 的時候，dockerhub(或其他 server) 就能提供使用者對應架構的 image 。

## 方法概念
docker 裡有 [buildx 這個工具](https://docs.docker.com/buildx/working-with-buildx/)，剩下的核心內容就是：
1. 指定要 build 出甚麼架構平台的 image，輸入 `docker buildx inspect` 確定有甚麼選擇 (這是 M1 的輸出)
    ```
    ❯ docker buildx inspect
    Name:   buildx-node
    Driver: docker-container

    Nodes:
    Name:      buildx-node0
    Endpoint:  desktop-linux
    Status:    running
    Platforms: linux/arm64, linux/amd64, linux/riscv64, linux/ppc64le, linux/s390x, linux/386, linux/mips64le, linux/mips64, linux/arm/v7, linux/arm/v6
    ```
    確定 platform 的選擇之後，後續在 build 的時候使用 --platform 這個 flag 就能指定架構了，例如：`--platform linux/amd64,linux/arm64`
2. 指定 multi-stage build 的時候使用目前機器預設使用的架構，在 Dockerfile 裡使用 `FROM --platform=$BUILDPLATFORM`，這意思是希望執行操作時是使用機器原生的平台來操作，這樣就不怕有不預期的影響。那為甚麼我們平常不用指定，現在要指定呢？這是因為平常 docker build 這個指令裡根本不會輸入多種環境，本來就是預設平台，所以在 Dockerfile 裡寫不寫就沒差了。
3. 利用 $TARGETPLATFORM 來知道現在這個 stage 是要準備哪個平台的 image layer。在第二點裡面我們讓 multi-stage 裡使用原生平台來跑 RUN 指令，但是事實上那個 stage 需要建構的是別的平台。而這個時候已經沒辦法使用 `uname -m` 來判斷架構了，所以需要透過 $TARGETPLATFORM 來判斷。
4. base image 也要支援多架構 (這個還好，base image 應該本來都會支援大部分的架構)

## Dockerfile 範例
```
FROM --platform=$BUILDPLATFORM alpine:3.14 AS base

ARG TARGETPLATFORM
ARG BUILDPLATFORM
ARG KUBECTL_TAG=v1.19.12

RUN apk add curl

FROM --platform=$BUILDPLATFORM base AS kubectl
# reference: https://www.downloadkubernetes.com/
RUN case ${TARGETPLATFORM} in \
  "linux/amd64")    ARCH="amd64";  ;; \
  "linux/arm/v6")   ARCH="arm";  ;; \
  "linux/arm/v7")   ARCH="arm";  ;; \
  "linux/arm64/v8") ARCH="arm64";   ;; \
  "linux/arm64") ARCH="arm64";   ;; \
  esac && \
  curl -o /usr/bin/kubectl -L https://dl.k8s.io/release/${KUBECTL_TAG}/bin/linux/${ARCH}/kubectl && \
  chmod +x /usr/bin/kubectl


FROM alpine:3.14
WORKDIR /workspace
# I love completion
RUN apk add bash bash-completion
COPY --from=kubectl /usr/bin/kubectl /usr/bin/kubectl
RUN echo 'source /etc/profile.d/bash_completion.sh' >> ~/.bashrc && \
    echo 'source <(kubectl completion bash)' >> ~/.bashrc 
```
build 指令範例：`docker buildx build --platform linux/amd64,linux/arm/v7,linux/arm64,linux/arm/v6 --progress=plain -t conanchang/kubectl --push .`
 - `--platform` 指定最常用的 amd64, M1 用的 arm64，還有其他沒用的當例子附上
 - `-t conanchang/k8s-cli --push` 是把結果打上 dockerhub 的 tag，也會進行推送。`--push` 不寫的話就單純 build 完，不會有結果喔(docker 也會提醒你這件事)
 - `--progress=plain` 如果剛開始嘗試這種多架構的 image build，可以把 output 都印出來看比較清楚

這個 Dockerfile 分為兩個 stage，第一個 stage 是利用預設的架構平台把各個要建構的平台所需要的 kubectl 指令下載下來。判斷的方式是看 $TARGETPLATFORM。btw，寫文章的當下 GKE 預設版本是 1.19，kubectl 也使用了對應的版本，如果你剛好有需要也可以改版本。

## 總結
其實整件事情概念是很簡單，使用 buildx 也能把 image manifest 的內容(有興趣的話就再繼續探究下去吧)藏起來不用管，麻煩的是多架構這件事情本身融合了 docker image 裡。在執行操作與架構 image 這件事情要小心處理。像我一開始沒弄清楚，跟著官網在 Dockerfile 裡使用 `FROM --platform=$BUILDPLATFORM`，結果 `uname -m` 就只會回傳 linux/amd64，讓各個架構的 image 都使用了 amd64 的 binary。不過多寫個幾次應該會得心應手了。