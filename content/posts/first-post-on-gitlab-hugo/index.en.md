---
title: First post on gitlab page with hugo
subtitle: OMG, it is amazing
date: 2021-02-11
categories: ["share"]
---

>Engineer should have a blog

Yes, I start to do it last year, using the Github page with Jekyll. I can't believe that I cannot get everything done in one day, install Jekyll on my laptop, pick a nice theme, make it build on the Github page. It takes me a lot of time before I can write the first post, and then I have to spend more time to learn how to make multiple language site, comment system...

Luckily, I have to learn about GitLab currently. When the first time I look at the default template of GitLab, I noticed that there is a one-click Hugo(I  know this tool when I surveyed some blog tools) template. It was amazing to make everything done after the first pipeline was built.

After that, I spent about 2 hours the learn the setting of multiple languages and the comment system. It seems everything is done, let go of writing.
