---
title: 原來 Gitlab page 寫 blog 這麼好上手
subtitle: 一個範本就能解決的事
date: 2021-02-11
categories: ["share"]
comments: true
---

>工程師都應該寫 blog

我是蠻同意的，所以在去年就有開始嘗試去做，而當時是用 Github page + Jekyll 來做。不過整體難度完全超過我想像，我原本以為大概是幾個一頁式的教學就能搞定的事情，結果我在一天內都沒有辦法開始寫第一篇文章。安裝 Ruby, Jekyll, 挑個像樣的版面，設定 Github page 這些加起都花了不少時間，而且後來花了點時間來研究多語言的設定和留言系統都相繼失敗。

最近是因為工作需求也開始在看 GitLab。GitLab 有提供一些範本幫助使用者可以快速建立一個 Project，我是第一時間就被 Hugo 吸引到了。因為當初在研究用甚麼工具寫 blog 的時候就已經知道 Hugo，沒想到開完 Project, 再 gitlab-ci build 一下就看到一個完整的網站了。

後面的設定都沒有卡過關，大概花了兩小時就把多語系和留言系統弄好了，真的超爽的。另外因為 GitLab 本身就有 IDE (我其實不太確定這能不能叫 IDE，因為他沒有 complie/run 的功能)，直接在這個 editor 裡面直接上就很舒暢了。以後就～好好寫吧
