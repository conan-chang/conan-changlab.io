---
title: Install gitlab runner on M1 Mac
subtitle: the native way
date: 2022-03-05
categories: ["learning"]
tags: ["gitlab-runner", "docker", "mac", "mac-m1"]
comments: true
---

## Introduce
If you want to run gitlab ci job/pipeline, you should have a GitLab runner. You can use shared runner on `gitlab.com`, or any shared runner on you self-hosted GitLab.

What if the shared-runner cannot satisfy you, maybe you want to get more interactive with the runner, get the runner more closely to your developer environment, your maybe want to set up your own runner.

This article was showed how I set up GitLab runner on my M1 MacBook. Since GitLab [supported M1 in 14.8](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26561?fbclid=IwAR30hZw0HwPbWUOStpkKG6mwQVIarA2ffC2pqozSJOZUEKF0lXcx81RKNxg). We can just run it natively with using docker executor.

## Install
You can just follow the instruction of [GitLab official docs](https://docs.gitlab.com/runner/install/osx.html)  
Because the time I wrote this post, the M1 version of GitLab runner was just released. We can just download the latest version:
```shell
brew install gitlab-runner
brew services start gitlab-runner
```
I am not using the official recommended way to install, because I found that it would not work. When I start the runner, GitLab show that the runner is never contact to GitLab.  

### Register runner
Here is the command I used:
```shell
gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "go-to-your-group-or-project-setting-cicd-runner-to-find-this-value" \
  --registration-token "go-to-your-group-or-project-setting-cicd-runner-to-find-this-value" \
  --description "docker-runner" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock
```
Because I want my docker can just run docker command like what I do on my MacBook, so you can see that I bind the docker socket to job container.  

### Test the runner
Here is the .gitlab-ci.yml I used to test if this runner can run docker command:
```yaml
default:
  image: docker
stages:
  - test
job:
  stage: test
  tags:
    - "docker"
  script:
    - docker info
```
And it worked. But I can find that there is some bug, for example if I run `gitlab-runner status`, it broked:
```shell
❯ gitlab-runner status
Runtime platform                                    arch=arm64 os=darwin pid=25604 revision=c6e7e194 version=14.8.2
gitlab-runner: "launchctl" failed with stderr: Could not find service "gitlab-runner" in domain for port
❯ gitlab-runner read-logs
Runtime platform                                    arch=arm64 os=darwin pid=27133 revision=c6e7e194 version=14.8.2
error reading logs timeout waiting for file to be created
```
I don't know why, so that I go back to use docker runner with docker executor.

## uninstall
Unregister the runner first:
```shell
gitlab-runner unregister --all-runners
```
Uninstall the tools itself:
```shell
brew uninstall gitlab-runner
```