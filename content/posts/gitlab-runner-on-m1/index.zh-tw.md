---
title: 在 M1 Mac 上安裝 gitlab runner
subtitle: 原生的方式
date: 2022-03-05
categories: ["learning"]
tags: ["gitlab-runner", "docker", "mac", "mac-m1"]
comments: true
---

## 簡介
一般而言如果你是在使用 gitlab ci 的話，你可能會使用平台上的 shared runner。但如果你想要讓 runner 更靠進自己，例如更好的 debug，更好的利用你本機的 cached 的話，你就可能要在自己的開發機上安裝 gitlab runner 了。

這篇文章就因為 GitLab 在 14.8 發佈了[支援 M1 的 runner](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26561?fbclid=IwAR30hZw0HwPbWUOStpkKG6mwQVIarA2ffC2pqozSJOZUEKF0lXcx81RKNxg)，所以就來在 M1 上體驗一下在原生的 gitlab runner 吧。

## 安裝
官網的教學在這裡：https://docs.gitlab.com/runner/install/osx.html  
因為在寫文章的當下，這個版本才剛發佈，所以就直接下最新版就好了：
```shell
brew install gitlab-runner
brew services start gitlab-runner
```
你可以發現我沒有用官網建議的方法安裝，因為我試了，但是不能用。裝完之後會在 GitLab 平台上說這個 runner 是 Never contact 的，然後不管看 log 看 status 的指令都是有問題的。所以我也懶得再研究下去了，最後用 homebrew 來安裝反而可以用。

### 註冊 runner 吧
指令:
```shell
gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "go-to-your-group-or-project-setting-cicd-runner-to-find-this-value" \
  --registration-token "go-to-your-group-or-project-setting-cicd-runner-to-find-this-value" \
  --description "docker-runner" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock
```
Because I want my docker can just run docker command like what I do on my MacBook, so you can see that I bind the docker socket to job container.  
這邊比較特別的是我把 docker bind 進 job 的 container 裡，這是因為我希望我的 job 能夠使用跟我開發一樣的 docker daemon。這對發有很大的幫助，想想你的 job 雖然在 container 裡面運作，但是 build 出來的 image 可以在本機直接運行，是不是很爽？

### 測試一下
簡單寫了個 `.gitlab-ci.yml` 來驗證，因為要確保 docker 能用，所以就直接跑了一個 docker 了:
```yaml
default:
  image: docker
stages:
  - test
job:
  stage: test
  tags:
    - "docker"
  script:
    - docker info
```
最終是能跑的，不過一些指令例如 `gitlab-runner status`/`gitlab-runner logs` 就會壞掉:
```shell
❯ gitlab-runner status
Runtime platform                                    arch=arm64 os=darwin pid=25604 revision=c6e7e194 version=14.8.2
gitlab-runner: "launchctl" failed with stderr: Could not find service "gitlab-runner" in domain for port
❯ gitlab-runner read-logs
Runtime platform                                    arch=arm64 os=darwin pid=27133 revision=c6e7e194 version=14.8.2
error reading logs timeout waiting for file to be created
```
有點小失望啦，這樣如果出問題 debug 也麻煩。所以我是回去繼續用 docker runner 吧

## 解除安裝
解除註冊:
```shell
gitlab-runner unregister --all-runners
```
刪掉 gitlab-runner 這個工具:
```shell
brew uninstall gitlab-runner
```