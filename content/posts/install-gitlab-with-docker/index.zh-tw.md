---
title: 一個 Docker container 裝完 GitLab 
subtitle: 最簡單的安裝方式
date: 2021-04-03
categories: ["share"]
tags: ["gitlab", "docker", "gitlab-runner"]
comments: true
---

## 會包含的內容有
 - GitLab 本體
 - 自己準備的 TLS 憑證
 - GitLab container registry
 - 對付 docker hub rate limit 的 proxy
 - Shared GitLab Runner 
 - GitLab Page
 - SSO(single sign-on)
 - Grafana

## 需求
一台 VM ，2 cores CPU, 8GB 記憶體, 40GB 硬碟，OS 用 Linux

## 安裝 GitLab 的不同方式
安裝 GitLab 其實有好畿種：https://about.gitlab.com/install/  
 - Linux package (apt/yum...)：package 的方式安裝，service 加 config 檔的方式做管理。用 CentOS 裝了一下，個人感覺不太好操作
 - Docker 版：也就是這篇。因為東西都在一個 container 裡頭跑起來，操作上很簡單，不過使用上總覺得卡卡的
 - K8s (Omnibus)版：用 GitLab 的 container image 安裝在 K8s 上，把 PostgreSQL 和 Redis 分離出來的運行方式：試了一下 GCP 上面的 Marketplace 版，把 DB 的 workload 分離出來會比較容易管理，不過用戶不多的情況下應該是不用太擔心
 - K8s (Helm)版：也是目前 gitlab.com 有在用的方式，利用 sub-chat 的方式把大量服務都組在一起，目前我用比較多的是這種。但是難度相對高，config 的方式比 Omnibus 複雜，操作／除錯要了解 K8s ，要去了解各個 sub-chat，還要設計資源的配置。不過優點很明顯，彈性很高，穩定性高，因為官方自己也在用。目前我用最多是這個版本，GKE/RKE/VMware Tanzu 都能裝起來  
其實細分下去組合有很多，不過大致上是這樣子

## 這次我用的環境
因為手上沒有 VM，也懶得開，這次就用 GCE 來做吧。
- OS 選擇 `CentOS 7`，這邊有個小插曲 COS (Container-Optimized OS) 因為沒有 `--privileged`，GitLab pages 會開不起來，害我白白浪費了兩個小時要重開一個 CentOS 來用
- CPU/Memory：我是用 `4`/`16`， 不過其實 `2`/`8` 也能正常運作
- 硬碟我開 `40GB`， 本來想要開 30GB 不過因為光 Image 就快2.4GB左右了，還有我會把 Runner 裝在同一台上， 想要留下來玩玩看別的東西也是蠻吃緊的
－ (GCE) 實驗用的機器把 preemptible 打開

## Omnibus 版的 config 方法
所有東西都放在 `gitlab.rb` 裡面，改完直接重啟 gitlab 就好了

### Docker 版正規做法
```bash
docker exec -it gitlab vim /etc/gitlab/gitlab.rb
docker restart gitlab
```
這個指令也能在 logs 裡面看到
```bash
[root@gitlab-docker ~]# docker logs gitlab | head -n 10
cat: /var/opt/gitlab/gitlab-rails/VERSION: No such file or directory
Thank you for using GitLab Docker Image!
Current version: gitlab-ee=13.10.2-ee.0
Configure GitLab for your system by editing /etc/gitlab/gitlab.rb file
And restart this container to reload settings.
To do it use docker exec:
  docker exec -it gitlab vim /etc/gitlab/gitlab.rb
  docker restart gitlab
```

### Docker 版 pre-config 的做法
因為當了一段時間 SRE 之後就會想要把步驟都寫在 script 裡頭，所以比起改 `gitlab.rb`(一堆 sed，或者拿預先準備好的檔案去覆蓋)，我更傾向用 pre-configure(docker env)的方法把 configuration 帶進去([ref](https://docs.gitlab.com/omnibus/docker/#pre-configure-docker-container))。這種做法會比較奇葩一點，因為你需要 docker kill/stop 之後再 docker start，而且這個改動是不會出現在 `gitlab.rb` 上面的，所以要怎麼改就見人見智了

## 實際來 config 吧
我是用 pre-configure 的方法，就是一直把 configuration 疊進去，格式會像下面這樣
```bash
GITLAB_OMNIBUS_CONFIG="the first line of configuration;"
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} the second line of configuration"
```
反正到最後就是會得到一個很長的 `GITLAB_OMNIBUS_CONFIG`

### 網址
```bash
GITLAB_OMNIBUS_CONFIG="external_url 'https://gitlab.conan-gitlab-docker.conanc.fun/';"
```

### 使用自己的 SSL certificate
GitLab 是支援 let‘s encrypt 的，~~(TL;DR)不過當然有很多原因都會讓我們選擇使用自己準備的 certificate 。 公司政策，或者詳情可以查 letsencrypt vs paid 也能略知一~~二  
做法如下，把 let‘s encrypt disable 之後
```bash
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} letsencrypt['enable'] = false;"
```
然後 gitlab 就會根據你的 domain 自動去找你的 SSL certificate，檔名會是：
- certificate: gitlab.DOMAIN.crt
- key: gitlab.DOMAIN.key

以我為例子，我的 domain 是 `conan-gitlab-docker.conanc.fun`，certificate 就會是 `gitlab.conan-gitlab-docker.conanc.fun.crt`

### 把 http 導向 https
來源：https://docs.gitlab.com/omnibus/settings/nginx.html  
其實 GitLab 裡頭還有像 registry 的功能，不過像 docker pull 預設是只能用 https ，所以我也沒有想要去設定了
```bash
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} nginx['redirect_http_to_https'] = true;"
```

### container registry 
這邊指定了 registry 的 url，以後像是 docker push/pull 都會對這個 server 做操作
```bash
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} registry_external_url 'https://registry.conan-gitlab-docker.conanc.fun';"
```

### dependency proxy
因為之前 Docker hub 增加了 rate limit 了嘛，相信各位都有各種手段解決。在 GitLab 裡頭也有用 proxy cache 的方法 ([ref](https://docs.gitlab.com/ee/user/packages/dependency_proxy/))。這個需要在裝 GitLab 的時候也要把功能打開：https://docs.gitlab.com/ee/administration/packages/dependency_proxy.html
```bash
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} gitlab_rails['dependency_proxy_enabled'] = true;"
```

### SSH port
有在管理 git server 的人都會遇到一個問題，就是操作用的 SSH port 會跟 git SSH port 衝到。解決方法可以是它們其中換到別的 port，或者在 server 前面用 port forwarding 或類似的方法解決。這裡簡單一點用前面那種，把 git SSH port 改換就算了
```bash
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} gitlab_rails['gitlab_shell_ssh_port'] = 30022;"
```

### Grafana 
GitLab 是有自帶 prometheus 的，不過 Grafana 要另外開起來才會有。認證方法當然有很多種，可惜的是沒有直接繼承 GitLab 的認證(好像有點太高級XD)，不過 GitLab 是可以當作 SSO 的來源，但是就太麻煩了。這裡直接設定個 admin 密碼就好，同時也要把 login 畫面開出來。下面指令是自己產生出一組亂數密碼，想要自己定義也是能把它改掉就好
```bash
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} grafana['disable_login_form'] = false;"
export GRAFANA_PASSWORD="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 20 | head -n 1 | tr -d '\n')"
echo ${GRAFANA_PASSWORD}
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} grafana['admin_password'] = '${GRAFANA_PASSWORD}';"
```

### Gitlab pages
跟 GitHub pages 一樣，GitLab 是有提供類似的功能的，個人覺得 GitLab 的比 GitHub 的更好用。這裡我們會把 http 導向 https，指定 SSL 憑證的路徑。這邊注意的是因為各個 page 會從我們指定的 domain name 展出各個 pages 的 url，所以憑證會是另外一個 wildcard: https://docs.gitlab.com/ee/administration/pages/#wildcard-domains-with-tls-support。另外因為我們是用 docker 運行 gitlab pages，需要設定 `inplace_chroot`: https://docs.gitlab.com/ee/administration/pages/#additional-configuration-for-docker-container
```bash
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} pages_external_url \"https://gitlab-page.conan-gitlab-docker.conanc.fun/\";"
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} pages_nginx['redirect_http_to_https'] = true;"
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} pages_nginx['ssl_certificate'] = \"/etc/gitlab/ssl/gitlab_page.crt\";"
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} pages_nginx['ssl_certificate_key'] = \"/etc/gitlab/ssl/gitlab_page.key\";"

GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} gitlab_pages['inplace_chroot'] = true;"
```

### 使用 GCP 做 SSO
GitLab 是有各種 SSO 的選擇的：[https://docs.gitlab.com/ee/integration/omniauth.html](https://docs.gitlab.com/ee/integration/omniauth.html)  
這裡用 GCP 的來用用看，有在用 google workspace 的話也適用，流程要跟著這篇的介紹產生出 oauth: [https://docs.gitlab.com/ee/integration/google.html](https://docs.gitlab.com/ee/integration/google.html)  
另外，GitLab 預設會讓新註冊的用戶進入 blocked 的狀況，需要被管理員 approve 才能加入，但是這就很不 SSO 了，所以會把 block 取消  
```bash
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} gitlab_rails['omniauth_allow_single_sign_on'] = ['google_oauth2'];"
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} gitlab_rails['omniauth_providers'] = [{\"name\" => \"google_oauth2\",\"app_id\" => \"YOUR_ID.apps.googleusercontent.com\",\"app_secret\" => \"YOUR_SECRET\",\"args\" => { \"access_type\" => \"offline\", \"approval_prompt\" => '' }}];"
GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG} gitlab_rails['omniauth_block_auto_created_users'] = false;"
```

## 安裝嚕
### 先把 SSL 憑證放好
以我的例子，我會有三組憑證
- `gitlab.conan-gitlab-docker.conanc.fun.crt`/`gitlab.conan-gitlab-docker.conanc.fun.key`
- `registry.conan-gitlab-docker.conanc.fun.crt`/`registry.conan-gitlab-docker.conanc.fun.key`
- `gitlab_page.crt`/`gitlab_page.key`
把它們的放到未來會 mount 給 container 的路徑上
```bash
export GITLAB_HOME=${HOME}/srv/gitlab
mkdir -p ${GITLAB_HOME}/config/ssl
sudo cp ./* "${GITLAB_HOME}/config/ssl/"
```

### 啟動 GitLab
參考：[https://docs.gitlab.com/omnibus/docker/#install-gitlab-using-docker-engine](https://docs.gitlab.com/omnibus/docker/#install-gitlab-using-docker-engine)，寫這篇的時候最新的 image 是 `gitlab/gitlab-ee:13.10.2-ee.0`
```bash
docker run --detach \
  --env GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG}" \
  --hostname team-cerberus.pentium.network \
  --publish 443:443 --publish 80:80 --publish 30022:22 --publish 8093:8093 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
   --privileged \
  gitlab/gitlab-ee:13.10.2-ee.0
```
等一陣子之後就會啟動了
```bash
[root@gitlab-docker ssl]# docker ps
CONTAINER ID        IMAGE                                  COMMAND                  CREATED             STATUS                       PORTS                                                                                     NAMES
3f6590440d4b        gitlab/gitlab-ee:13.10.2-ee.0          "/assets/wrapper"        1 hours ago         Up About an hour (healthy)   0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp, 0.0.0.0:8093->8093/tcp, 0.0.0.0:30022->22/tcp   gitlab
```
接下來從瀏覽器進入 GitLab，我這邊的 URL 是 `https://gitlab.conan-gitlab-docker.conanc.fun`，進入之後就會設定管理員密碼，而帳號是 `root`。  
然後這個時候其實還沒有 Grafana 的，需要把它 enable：去 `admin area` -> `setting` -> `Metrics and profilling` 裡頭的 `Metrics - Grafana` 裡頭的 `Enable access to Grafana` 勾起來。然後重新整理 `Admin Area` -> `Monitoring` -> `Metrics dashboard` 就會出現了

### 安裝 GitLab runner
只有 GitLab 沒有 runner 大概有一半的 GitLab 功能都用不太到了，這邊展示一下安裝 GitLab runner 的方法，那其實 GitLab runner 有很多種 [executor](https://docs.gitlab.com/runner/executors/README.html)，這裡就以 docker 來使用。這樣執行的 job 都會在 container 裡頭進行，得到很好的 isolation 特性，但是就不好使用 docker 了嘛  
關於這點 GitLab 也有介紹三種不同的方法解決，各有優缺：[https://docs.gitlab.com/ee/ci/docker/using_docker_build.html](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)  
這邊就用最簡單的 [docker binding](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding) 來使用 docker  

#### 首先是啟動 GitLab runner  
那這邊可以看到有把 volumn mount 進 container。(TL;DR)這邊有個題外話是我本來想要用用看 web terminal 的，結果弄完之後有時能用，有時會連線失敗。所以後來也沒搞了
```bash
docker run -d --name gitlab-runner --restart always \
     -v ${HOME}/srv/gitlab-runner/config:/etc/gitlab-runner \
     gitlab/gitlab-runner:alpine-v13.10.0
```

#### 再來是註冊  
首先要拿到註冊碼，方法是用 root 登入 GitLab，到 `Admin Area` -> `Overiew` -> `Runners` 就會看到用來註冊的 `URL`，還有 `token`  
接下來就是讓剛啟動好的 runner 註冊進來  
(這句不要重複執行，不然會註冊多個 runner )  
這裡我 mount 的路徑是在 `${HOME}` 底下，這裡是自己決定就好  
```bash
docker run --rm -v ${HOME}/srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:alpine-v13.10.0 register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "https://gitlab.conan-gitlab-docker.conanc.fun/" \
  --registration-token "h-FmSJ_KS8FS3SDFIL2F" \
  --description "docker-runner" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock
```
最後的 `--docker-volumes` 就是為了 docker binding 的部分  
另外也能看到 config 檔案的 volumes 多了這個路徑  
(其實也比較建議直接改 `config.toml`，如果不用 `--docker-volumes` 的話可以 `sed -i 's#"/cache"]#"/cache", "/var/run/docker.sock:/var/run/docker.sock"]#g' ${HOME}/srv/gitlab-runner/config/config.toml`)
```bash
[root@gitlab-docker ~]# cat ${HOME}/srv/gitlab-runner/config/config.toml
concurrent = 1
check_interval = 0
[session_server]
  session_timeout = 1800
[[runners]]
  name = "docker-runner"
  url = "https://gitlab.conan-gitlab-docker.conanc.fun/"
  token = "JwHEHEHEHEHEHEHEHEHE"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

## 總結
要自己安裝一個 GitLab 實在沒有很容易，因為功能真的很多，每個功能都要花一點點時間學怎麼用。  
所以其實如果團隊不大的話可以直接考慮 gitlab.com  
因為自己管理一個 gitlab 可以得到的好處其實不多
 - 資料隱私性：可以自行選擇網路環境
 - 災害救援速度：gitlab.com 壞了就只能等，掌控性會比較低
 - 網路速度：雖然 gitlab.com 是用 GCP 的，延遲是不低，但頻寬是比不上自行架設的快(是要多寬XD?)
 - 管理層級： gitlab.com 的使用者只能管理到 group，有些像 shared runner 的東西就不能自己設定了
 
壞處則是:
 - 要請一個員工管理：這個員工要懂的東西可能還不少，他有日常管理，災害復原，升版；GitLab 升版可能不是那麼簡單喔，因為它跟 `postgreSQL` 是有關連的，不過這也就更後面的話題了
 - 少許硬件費用(GitLab 本體)：說少許的原因 workload 都在 runner 上，那就跟使用 gitlab.com 是一樣的
