---
title: Loki 取代 Elasticsearch 收 Log 可行嗎
subtitle: 使用它的 Microservices 模式
date: 2022-06-11
categories: ["learning", "testing"]
tags: ["logging", "loki", "gke", "kubernetes"]
comments: true
---

## 簡介
Loki 是 Grafana 開發出來用來收 Log 的工具，基本上就是用來取代 Elasticsearch(ELK/EFK) 在收集 log 的用法，是一個可能輕鬆收集大量 log 的一個工具，不過就是犠牲了查尋 log 的效能。這篇文章會**簡單**介紹怎麼用 helm chart 安裝 microservices 架構的 loki (2.5版)，以及重點實測 Loki 在查詢方面到底夠不夠用。最後分享一下我怎麼看 Loki 和 Elasticsearch 的選擇。

## 準備環境
### GKE 環境
使用的是 GKE，簡單而言切開三個 node pool，分別是:
1. default pool: 放一般的 workload，像是負責模擬寫 log 的應用；跟一些 Loki 的小模組、Grafana
2. ingester: Loki 負責收 log 的模組 (write path 核心模組)
3. querier: Loki 負責查 log 的模組 (read path 核心模組)

會這樣切分是因為不想把 workload 混在一起而影響實驗的結果，像 promtail 也是會吃 CPU 的

更詳細的配置可以查看它的 terraform project: https://gitlab.com/conan-example/tf-gke-loki

### 安裝 loki
一些參數的調整，還有架構的選擇放在 [參數解釋](#參數解釋)

不過這裡面有個比較值得提的是 GCS 的認證是把一個 service account 的 key mount 進去再利用 [GOOGLE_APPLICATION_CREDENTIALS](https://cloud.google.com/docs/authentication/getting-started) 進行設定認證的。(這個東西我在 Loki 官網上沒有找到介紹，ㄏㄏ)

安裝指令如下: 
1. 因為資訊是丟 GCS 的，先來準備個 service acccount 的 credenials，IAM 要給 `Storage Admin`。下面指令是它的 Key 丟進 K8s 的 Secret 裡:
    ```shell
    kubectl -n loki create secret generic gcs-credentials --from-file=key.json
    ```

2. 使用官方的 helm chart 來安裝
    ```shell
    helm repo add grafana https://grafana.github.io/helm-charts
    kubectl create ns loki
    helm upgrade -n loki --install loki grafana/loki-distributed -f values.yaml --version 0.48.4
    ```
    完整 helm value: https://gitlab.com/conan-example/loki-promtail-grafana/-/blob/main/loki-distributed/values.yaml

    完成後應該會看到每個元件都起來:

    ```shell
    % kubectl get pod -n loki
    NAME                                                   READY   STATUS    RESTARTS   AGE
    loki-loki-distributed-compactor-6b78cb7984-kbx4d       1/1     Running   0          18m
    loki-loki-distributed-distributor-7cc78c78fb-lvrdv     1/1     Running   0          18m
    loki-loki-distributed-gateway-bc8bf4c58-mrw6t          1/1     Running   0          18m
    loki-loki-distributed-index-gateway-0                  1/1     Running   0          18m
    loki-loki-distributed-ingester-0                       1/1     Running   0          18m
    loki-loki-distributed-ingester-1                       1/1     Running   0          18m
    loki-loki-distributed-querier-6f9f8cd678-l9ldg         1/1     Running   0          18m
    loki-loki-distributed-query-frontend-d55765ff5-fxwcp   1/1     Running   0          18m
    ```

### 安裝 Promtail
就是把 log 收集起來丟給 Loki 的工具，這邊因為沒有要對 log 做太多處理，所以大部份使用[預設設定](https://github.com/grafana/helm-charts/blob/promtail-5.1.0/charts/promtail/values.yaml)。主要是把 log level 拉出來做 index，不過其實 caller 也蠻值得做加的，不過那不是重點就先跳過。主要的 config 如下:
```yaml
pipeline_stages:
- cri: {}
- match: 
    selector: '{app=~"log-.*"}'
    stages:
    - json:
        expressions:
            level: level
    - labels:
        level: level
```
完整 helm values.yaml: https://gitlab.com/conan-example/loki-promtail-grafana/-/blob/main/promtail/values.yaml

使用官方的 helm chart 來安裝:
```shell
helm -n loki upgrade --install promtail grafana/promtail -f values.yaml --version 5.1.0
```

### 安裝 Grafana
這邊把 datasources 先寫好在 config 裡面，比較特別的是 timeout 我先設了 10 分鐘，不過真正的 timeout 其實任何一個環節都有可能是撞到 (下面 [害死新手的 timeout](#害死新手的-timeout) 會介紹)，grafana 這邊就先設的夠大
```yaml
datasources:
  datasources.yaml:
    apiVersion: 1
    datasources:
      - name: Loki
        type: loki
        access: proxy
        url: http://loki-loki-distributed-gateway.loki
        jsonData:
          maxLines: 1000
          timeout: "600"
```
完整 helm values.yaml: https://gitlab.com/conan-example/loki-promtail-grafana/-/blob/main/grafana/values.yaml

1. 透過官方 helm chart 安裝
    ```shell
    helm upgrade --install -n loki grafana grafana/grafana -f values.yaml --version 6.29.6
    ```
2. helm 安裝完會提示你拿 admin 密碼
    ```shell
    kubectl get secret --namespace loki grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
    ```

### 部署摸擬產生 log 的 Deployment
這裡使用的程式是: [https://hub.docker.com/r/conanchang/crazy-log-generator](https://hub.docker.com/r/conanchang/crazy-log-generator), 簡單而言就是一個會不斷產生 json 格式 log 的小工具，範例如下:
```json
{"level":"info","ts":1654603607.5484238,"caller":"app/repeat.go:35","msg":"OFRfNRi rAfHS ALEMObElYpsTsG ZjvBRyhbA hOZMhWUhsYgC bDyqIcUYv gnZPNf lexDQ uNlJZIgAzquiU NtvSgdfoYKuO"}
```  
接下來會使用好幾種規模的應用來模擬各種情景，寫 log 的頻率都是每寫完 500 行休息 1 秒。這是模擬一般 application server 大概可以給 500 RPS 左右的服務能力。不同規模的應用就用不同 replicas 來模擬，分別有:
1. replicas: 2
2. replicas: 6
3. replicas: 18

還有另一個類型是每寫完 10000 行休息 5 秒，12 個 replicas

### 參數解釋

這邊不會針對 component 解釋(因為我也沒有很懂)，如果忘記的話可以去官網複習: https://grafana.com/docs/loki/latest/fundamentals/architecture/components/。比較特別的是這次實驗用的 helm chart 有帶一顆 gateway(nginx)，算是 write/read path 的統一入口。

Loki 參數其實大部份都是使用預設的，不過還是有些值得留意的參數。  
跟效能有關的:
- `chunk_cache_config`:

  首先是架構方面，針對 querier 對於 chunk 的 cache，可以選擇放 memcached 或者放在它自己身上的 fifocache。這邊我沒有選擇 memcached 是因為它是一個 statefulset，使用 loki 就是希望省錢，需要 query 的時候等一下下自動擴展機器出來就能用了，放一個 memcached 有點奇怪。再來 redis 我也沒有去嘗試了。  
  另外沒有用 memcached 還有另一個問題是，我用下去的時候 querier 狂噴跟 memcached 互動的 error，實在沒空去追原因  
  所以 `chunk_store_config.chunk_cache_config` 我是有開了 fifocache 來保留 chuck 的 cache。至於會設 12Gi 是因為這個 12Gi 加上 `results_cache` 那 1Gi 會讓 quierier 用到大概 26Gi，可能是 GC 需要多一倍記憶體做 buffer 吧，這個有點太深入我就沒有研究下去了。

- `chunk_encoding`，這裡編碼有:

  1. `gzip`，壓縮率最高，但是最會吃 CPU ，存進 GCS 的東西不要計較那些壓縮率，東西拿出來看才是重點。
  2. `lz4`，真是太雷了，因為要模擬真實使用，所以有兩個 ingester 來降低 downtime。結果它硬生生多存了 30% 左右重複的 log，query 的時候要浪費資源去爬，去重複。
  3. `snappy`，我是使用 `snappy` 的，雖然沒有跟 `gzip` 對比，不過搜尋速度理論上應該只能比 gzip 快吧。(該不會 gzip 檔案小，從 GCS 下載速度比較快就....)
  
- `frontend_worker.parallelism`:

  這個是 querier 裡面同時去拿 job 的數量，一般建議是使用 CPU 核心數，但是我發現這會讓它跑不滿 CPU，所以設了 CPU 核心數的雙倍。也就是 4 core 的機器，我把這個參數設到 8，跑好跑滿

- `limits_config.max_query_parallelism`:

  如果我沒理解錯的話，是用來控制同一個 query 它可以同時被切出去多少個 queries，就是預防多人一起使用的時候一個人用光所有資源；或者一下次撒出去太多 query，但是很快達到 return limit (查到1000筆就停手)的時候造成效能浪費吧。這邊為了儘可能用滿所有效能，所以就設了 32，也就是 4 個 querier * 8 個 frontend_worker.parallelism 的量，這邊不用考慮多個人同時使用吼，因為榨乾效能才是對所有人都最好的  
  而這個參數再往上應該我猜也許可能沒意義吧，因為 frontend_worker.parallelism 已經有超賣一倍了，沒有必要在這邊再調整 max_query_parallelism

- `limits_config.split_queries_by_interval`:

  用來設定一個切分成小 quiery 的 time windows 大小，理論上越大，query 越不碎片化。但是如果碎片化會讓 Loki 變慢太多的話，那它這個 microservice 的架構還有調整的空間。但是設太大，又可能造成切完 query 之後，數量填不滿我的 16 cores CPU。這邊就估算個最常用的搜尋範圍是 24h 好了，我把 `querier.query_ingesters_within` 設成 `90m` 就能切成 16 個任務，剛好塞滿 16 cores。再大就會造成資源浪費了，多人同時使用就排隊唄。

  這個數字有點沒有標準，其實就看你怎麼取捨，我設定最常用的搜尋範圍是 24h 是因為如果今天要搜的範圍:
   - 比 24h 小，例如 6h 內的 log 好了，那就是切 4 個 query 出去，應該也不會慢到哪裡
   - 比 24h 大，例如 48h 內的 log 好了，那就是切 32 個 query 出去，那就是排隊而已
  這樣聽起來是不是設小一點就無敵了？其實就是碎片化帶來的問題，像是我們沒用到 memcache 來存 chunk，querier 又沒有共用記憶體，那就會變慢。而且比較基礎的是工作不連貫就會卡卡的。所以這個參數就是看使用場景來調就好

另外跟 ingester 相關的就沒有太研究了，因為很單純。唯一要擔心的是 ingester 這個 write path 也會吃記憶體，大概是 chunk buffer，所以 log 太多應該會 OOM, 不過預設設定在我這個場景只會讓兩顆 ingester 分別使用 5 GB 左右記憶體。所以就沒有做太多調整了

#### 害死新手的 timeout
這大概是剛開始用 Loki 覺得"這工具真破"的地方，在進行大面積的搜尋的時候，會遇到 timeout。解決辦法是這樣:
1. 建 Grafana 的 helm values 裡的 ingress 裡我加入了 proxy-read-timeout。反正如果是別的 ingress controller 記得改就是了，算是一些基本操作:
    ```yaml
    ingress:
      annotations:
        nginx.ingress.kubernetes.io/proxy-read-timeout: "600"
    ```
2. Grafana 連 Loki 這個 datasource 要把 timeout 調大，以下是 Grafana 的 helm value:
    ```yaml
    datasources:
      datasources.yaml:
        apiVersion: 1
        datasources:
          - type: loki
            jsonData:
              timeout: "600"
    ```
接下來其實也是跟 timeout 相關，不過有點偏 Loki 自身元件的設定
- 502 的問題，解決方法改 Loki config (參考: [https://github.com/grafana/loki/issues/4015#issuecomment-1035506622](https://github.com/grafana/loki/issues/4015#issuecomment-1035506622)):
    ```yaml
    server:
      http_server_read_timeout: 10m
      http_server_write_timeout: 10m
    ```
- 然後會遇到 504 的問題，解決方法是把 gateway 的 timeout 加大，在 helm value 裡加 (參考:[ https://github.com/grafana/loki/issues/4015#issuecomment-1035656126](https://github.com/grafana/loki/issues/4015#issuecomment-1035656126))
    ```yaml
    gateway:
      nginxConfig:
        file: |
          http {
            proxy_read_timeout    600s;
            ...
    ```
最後 Loki config 裡有個我也看不太懂的 timeout，反正它叫 timeout 我就給它加大就是了:
```yaml
    querier:
      query_timeout: 10m
      engine:
        timeout: 10m
```

## 測試結果
其實都是邊調參數邊做測試的，沒有做太嚴謹的測試。不過就是根據上述的場景以及參數的設定，目前使用 Loki 2.5 版:  

寫入: 2*2 cores 的 ingester: 每天寫入約 240G 資料， 可以說是毫無壓力 (這是當然的，也是 Loki 的設計目標)

讀取:  
因為 HPA 的設定是 1 -> 4，所以會順便看一下 1 * 4 cores 的 querier 和 4 * 4 cores 的 querier 提供的搜尋性能，而搜尋用的 query 是最簡單的關鍵字查詢，例如:
```shell
{namespace="default"} |= "youneverfindme"
```
如果是要測試看一些小型的服務則是，log-1/log-2/log-3 分別對應是上面提到 Deployment replicas 的 2/6/18
```shell
{app="log-1"} |= "youneverfindme"
```

另外要注意的是目前 querier 總共記憶體有 32 * 4 = 128GB，也就是理論上如果當下爬的資料範圍都能全部丟進記憶體的話，速度在第 2, 3 次之後的速度也會比第 1 次快。所以測試方法會是這樣:

1. 4 台機器已經透過 node pool 的自動擴展事先開好
2. HPA 冷卻後，把 querier 的 Pod 刪光讓記憶體清空進行一次查詢 (這就不多做幾次取平均了，太累)
   1. 如果搜尋太久的話，新加入的 querier 其實也會幫忙查
3. 理論上同樣的 query 多查幾次的話因為 chunk 都已經進入記憶體了嘛，所以速度也會比較快。在一些反複查詢的場景也有可能會遇到，這會測 3 次取平均

測試結果:
 - 整個 cluster `{namespace="default"}` 都搜尋的:

    |重複|範圍|資料量|行數|總共CPU|時間|處理速度|
    |---|---|---|---|---|---|---|
    |否|24h|238 GB|1379090176|4 cores|204s|1.17 GB/s|
    |否|24h|238 GB|1379515416|16 cores|74.4s|3.20 GB/s|
    |是|24h|238 GB|~同上|16 cores|(72+73.8+66.6)/3 = 70.8s |(3.32+3.22＋3.57)/3 = 3.37 GB/s|
    |否|12h|119 GB|690448910|4 cores|69s|1.72 GB/s|
    |否|12h|119 GB|689128575|16 cores|41s|2.90 GB/s|
    |是|12h|119 GB|~同上|16 cores|(38.6+40.1+39.1) = 39.2s|(3.08＋2.96＋3.04)/3 = 3.02 GB/s|
    |否|3h|29.4 GB|170456595|4 cores|36.4s|0.81 GB/s|
    |否|3h|29.1 GB|168801051|16 cores|14.6 s|2.02 GB/s|
    |是|3h|~同上|~同上|16 cores|(10.9+10.6+10.4)/3 = 10.6s|(2.68+2.74+2.80)/3 = 2.74 GB/s|

     - 在測試這種大規模的爬 log 的時候有發現有一個有趣的現象，因為爬很久，所以透過 HPA 加入的 querier 其實在中間也會加入戰局，所以事實上面寫的花費的時間後面一部分時間都是有 16 cores 的，所以不用覺得奇怪 16 cores 沒有得到 4 cores 的 4 倍效能 (同理也可以觀察到 3h 時間搜尋時間比較短，後面的 querier 加入戰局比較慢就真的只有在用 4 cores 就查了；或者機器已經縮回去的時候一樣要做機器的 scale out 的時候也會這樣)  
     - 另外前面也有提到目前設定是最適合 24h 使用的，像 12h 的切分會沒有辦法搾乾 16 cores 的效能 12h/90m = 8 個任務，時間越短效率看起來就越不好。那是一個取拾，不過時間短的 query 本來就比較快跑完，所以就拿它們來犧牲合情合理
     - 另外在查 24h 的第一次只有到 1.17 GB/s 的效率，而 12h 的第一次卻有 1.72 GB/s 。這應該就不是誤差了，不過不知原因為何
 - 查詢一天會丟 102 GB 的應用 `{app="log-3"} |= "youneverfindme"`:

    |重複|範圍|資料量|行數|總共CPU|時間|處理速度|
    |---|---|---|---|---|---|---|
    |否|24h|102 GB|592902812|4 cores|60.6s|1.68 GB/s|
    |否|24h|102 GB|592258259|16 cores|28.6 s|3.57 GB/s|
    |是|24h|102 GB|~同上|16 cores|(26.4+27.6+25.8)/3 = 26.6 s|(3.89+3.70+3.97)/3 = 3.85 GB/s|
    |否|12h|50.9 GB|295573139|4 cores|53.6 s|957 MB/s|
    |否|12h|50.9 GB|295581389|16 cores|17.1 s|3.03 GB/s|
    |是|12h|50.9 GB|~同上|16 cores|(14.7+13.6+14.2)/3 = 14.1 s|(3.49+3.78+3.62)/3 = 3.63 GB/s|
    |否|3h|12.6 GB|73138099|4 cores|15.4 s|0.83 GB/s|
    |否|3h|12.5 GB|72602831|16 cores|5.61 s|2.34 GB/s|
    |是|3h|~同上|~同上|16 cores|(4.18+3.76+4.68)/3 = 4.2 s|(3.11+3.46+2.72)/3 = 3.09 GB/s|

     - 可以看到第一次測試的時候有拿到 1.68 GB/s 的效率，這跟上一次個測試 12h 的 1.72 GB/s 比較接近。可以發現如果 query 會觸發 scale out 的話，後面 querier 甚麼時候加入也是蠻看運氣的
     - 總資料量開始低於總記憶體量的狀況下效率也比較好
     - 跟上個測試一樣，時間範圍越低，效率也預期的越差 24h: 3.85GB/s；12h: 3.63GB/s；3h: 3.09GB/s


剩下 log-2(25.9GB/天) 和 log-1(8.68GB/天) 就懶得記錄了，因為狀況其實是類似的。一台 4 core 的 querier 大概可提供 0.7~0.9 GB/s 的爬取效率。剩下的就是簡單的數學了，想要多短的時間內查多大的 Log 就提供多少 CPU。  
以目前設定/機器:
 - log-1(8.66GB/天): 查詢一天的 log 可以在 5 秒內完成，資料進記憶體後可以 3 秒內
 - log-0(0.157GB/天): 查詢一天的 log 可以在 1.5 秒內完成，資料進記憶體後可以 0.5 秒內。不過時間低於 1 秒的變數有點太大，反正很快就是了
另外就是載 GCS 阿都會有個低消，所有 chunk 在進記憶體前都需要預期個 5 秒左右的等待才會查詢到嘿

## 價格跟 Elasticsearch 比較
就使用場景不同，這是一個蠻大的話題吼，下面就單純寫下我的看法
### Loki
對 Loki 最完美的 case 是 **收 Log 就為了稽核**，沒有看 log 需求的話  
Loki 花費為:
1. 2 台 2 cores 的機器配 16GB 記憶體 (16GB 是因為死了一台的話，另一台預計要撐 10GB 的資料，不過當然你可以讓 ingester 不要 buffer 太多，但是這點小東西就不要計較了嘛)
2. GCS 放 standard(反正對手存硬碟的不要太計較)，240GB * 365 天，operation B 我看 billing 有一百二十萬一天，我也不知我玩了多少，算 1,300,000 吧；operation A 我玩了不到六萬一天，就算六萬吧。

最後估價大約 $1,939 美元: https://cloud.google.com/products/calculator/#id=5eb4c8b8-e15a-47c3-a286-2c12952ed635 (不過 GCS 快要漲價了，如果儲存到多地區的話，現在是 $2,464(只比單 region 貴一點)，未來是 $9,536 美元，漲超多，怎麼可以這麼 78，難怪最近被罵爆)

### Elasticsearch

因為我也沒有管理過每天 240GB Log 的 Elasticsearch，以下看看就好


參考 Elasticsearch 的估算: https://www.elastic.co/blog/benchmarking-and-sizing-your-elasticsearch-cluster-for-logs-and-metrics  
把所有資料都當作 warm data (雖然我們都知道沒有 hot instance 用 SSD 不好，不過先這麼算吧)來看，簡單一點，反正查詢的話 Elasticsearch 不可能輸 Loki。  

Elasticsearch 花費為:  
首先總資料量為: (240GB * 365 * 2 replicas) * (0.15+0.1) = 219000 GB
1. 12 台 16 cores，128 GB 記憶體的機器。 12台的算法是 ROUNDUP(219000/160/128)+1台 ，其中 160 是記憶體與資料的比例
2. HDD 硬碟 219000 GB

最後估價大約 $16,457 美元，不過實際會比這個再貴不少: https://cloud.google.com/products/calculator/#id=d0321afa-9c9b-4024-9464-99aca4bb4c21

### Loki 省下來的錢可以跑怎樣的 query 呢
首先每月差額 $16,457 - $1,939 = $14,518。要運用這波錢呢要提到一個 Loki 很關鍵的設計，也就是 stateless，讓它可以使用 spot instance 來跑 querier。  
胡亂按了一下可以開 96 台 32 cores，128 GB 記憶體的 spot 機器，每天開 12 小時(週末不休息，狂看 log)。價錢是 $13,848，剩下就當作 operation 的錢: https://cloud.google.com/products/calculator/#id=42168174-bce6-490a-991c-1a9fce986b62  
這大概是可以把 48 天的 log 都載到記憶體裡面，跳過調參數的部份，假設效能是(Loki宣稱的)等比增長的話，96台*32cores = 3072 cores。今天的測試是用 16cores 達到 3.2G/s 的效率，那 3072/16 * 3.2G/s = 614.4 G/s 的效率。爬一波 30天的 Log 就是 240(GB)*30/ 614.4(GB/s) = 11.7 秒嗎


## 心得總結
因為想要知道 Loki 能不能拿來用吼，Grafana Lab 自己說 PB 級的 Log 用的很開心也不能儘信嘛你說是不是，所以還是測試了一波。那這個測試的標準其實算蠻高的了，因為一個應用不會日產 240 GB 的 log，但是因為過往大家習慣了無惱的查 Log，不會管現在要查哪一個元件的 Log。所以才會把標準設在每日產生超過 100GB log 這種等級。

事實上一個應用每天產個 1GB 已經差不多了，按照我們這個測試的設定，查詢好幾天的 log 應該都在 5 秒內完成。對比 Elasticsearch 的毫秒級當然沒法比，但是其實也許是能夠忍受，因為帶來的好處實在太多。再來就是一個很關鍵的事情就是 label 的設計，重新思考一下 logging 系統的使用也是很重要的

另外一個大問題是，今天測試的算是最簡單的 query 了，如果想要多用 LogQL 的其他查詢功能的話，不確定效能會變得多慢

然後個人覺得有以下考量點會覺得還是用 Elasticsearch 會比較好:
1. 錢: 多花錢能夠獲得更好的搜尋效率
2. 有沒有人可以去管理 Elasticsearch: 管過 Elasticsearch 都知道那系統時不時就會出一些問題要做一些調整。而且你要擔心的是 Elasticsearch 出問題的時候，丟 Log 的 client 像 Fluentd 也會跟著叫喔
3. 地端環境不夠大，沒有一個共用的 MinIO/Ceph 來提供 object storage，或者連機器自動擴展都沒有支持的話，使用 Loki 就沒甚麼好處了

簡單來說，如果管 Log collection system 的不是我，錢也不是我出的話，我會希望用 Elasticsearch。  

對於公司而言，除了錢跟人力，以下也是值得考慮用 Loki 的，雖然網路上已經有很多介紹了，不過還是寫一下我的想法:
1. Log 量是否很飄忽，需要一個可擴展的系統來收 Log
2. 是否因為一些像法規的問題需要一個可以存超過 3 年 Log 的工具
3. 殺雞就用雞刀，log 不多，可能每天 1 萬筆的話還是用個比較輕量的工具吧朋友

簡單而言著重儲存的選 Loki，著重搜尋選 Elasticsearch。一些跟 Prometheus 甚麼的整合，tracing 的我覺得都是後話了。