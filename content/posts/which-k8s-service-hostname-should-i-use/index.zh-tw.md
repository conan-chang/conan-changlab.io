---
title: 我應該用哪個 K8s service hostname 才對 ?
subtitle: 真相只有一個
date: 2021-02-13
categories: ["share"]
tags: ["kubernetes"]
comments: true
page:
    theme: "full"
---

## 選擇
假設你在的 K8s cluster 的 dns domain 是 `cluster.local` [ref](https://kubernetes.io/docs/tasks/administer-cluster/dns-custom-nameservers/#introduction)，而 namespace 是 `default`，service name 為 `service-name`  
這樣的話以下有四個 hostname 都可以用來連接到你的 service
 - service-name
 - service-name.default
 - service-name.default.svc
 - service-name.default.svc.cluster.local

但是你應該用哪個呢？

## 我的選擇
service-name.default.svc，原因是這樣的
 - service-name：只能在同一個 namespace 中使用
 - service-name.default：這個沒甚麼問題，但是我覺得多加上 svc 就能明確多了。知道那是指向 service 的，長的也像 K8s 內部的 A record 的組成
 - service-name.default.svc.cluster.local：kube-dns 換一個 domain name 就不能用了。雖然我沒有試過，不過既然加這個 domain name 沒有好處的話，當然也不用它了

---

## 以下是認真了解
以下個準備個 deployment 和 service(`go-http-server-service`)，用來測試 requests 的接收。(以下的東西可以用看的就好，不一定要跟著做才能理解)
```bash
ns=default  # you can change it
# apply a service and deployment(with path: /hello)
kubectl -n "${ns}" apply -f https://gitlab.com/conan-chang/example-code/-/raw/master/simple-go-http-server/k8s-deployment.yaml
```

開一個 Pod 來做測試，這裡使用 `alpine`，快速輕便。指令可以參考 [kubectl run](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#run)
```bash
kubectl -n "${ns}" run -it --rm --restart=Never simple-tool --image=alpine:3.13 sh
```
發一下 request
```bash
# we use curl here
apk add curl
curl go-http-server-service/hello
curl go-http-server-service.default/hello
curl go-http-server-service.default.svc/hello
curl go-http-server-service.default.svc.cluster.local/hello
```
會看到這樣的結果，全部都能通
```shell
/ # curl go-http-server-service/hello
hello
/ # curl go-http-server-service.default/hello
hello
/ # curl go-http-server-service.default.svc/hello
hello
/ # curl go-http-server-service.default.svc.cluster.local/hello
hello
```

### 答案
打開 `/etc/resolv.conf` 來看看，相信真相已經呼之欲出了
```shell
/ # cat /etc/resolv.conf
search default.svc.cluster.local svc.cluster.local cluster.local asia-east1-a.c.conanc.internal c.conanc.internal google.internal
nameserver 10.76.0.10
options ndots:5
```
在查找 dns 之前，先確定一下這裡個 10.76.0.10 是哪個 server，沒錯～就是 kube-dns
```shell
conan@cloudshell:~$ kubectl get service -A -o wide | grep 10.76.0.10
kube-system   kube-dns                 ClusterIP   10.76.0.10     <none>        53/UDP,53/TCP   15h    k8s-app=kube-dns
```

### TL;DR
再來用 `nslookup` 查 `go-http-server-service`，可以看到 `go-http-server-service.default.svc.cluster.local` 直接查到了
```shell
/ # nslookup -debug go-http-server-service
Server:         10.76.0.10
Address:        10.76.0.10#53
------------
    QUESTIONS:
        go-http-server-service.default.svc.cluster.local, type = A, class = IN
    ANSWERS:
    ->  go-http-server-service.default.svc.cluster.local
        internet address = 10.76.1.109
        ttl = 30
    AUTHORITY RECORDS:
    ADDITIONAL RECORDS:
------------
Name:   go-http-server-service.default.svc.cluster.local
Address: 10.76.1.109
------------
    QUESTIONS:
        go-http-server-service.default.svc.cluster.local, type = AAAA, class = IN
    ANSWERS:
    AUTHORITY RECORDS:
    ->  cluster.local
        origin = ns.dns.cluster.local
        mail addr = hostmaster.cluster.local
        serial = 1613232000
        refresh = 28800
        retry = 7200
        expire = 604800
        minimum = 60
        ttl = 60
    ADDITIONAL RECORDS:
------------
```
那理所當然的，如果在查 `go-http-server-service.default` 的時候 `go-http-server-service.default.default.svc.cluster.local` 就會找不到了
```shell
/ # nslookup -debug go-http-server-service.default
Server:         10.76.0.10
Address:        10.76.0.10#53
------------
    QUESTIONS:
        go-http-server-service.default.default.svc.cluster.local, type = A, class = IN
ANSWERS:
    AUTHORITY RECORDS:
    ->  cluster.local
        origin = ns.dns.cluster.local
        mail addr = hostmaster.cluster.local
        serial = 1613232000
        refresh = 28800
        retry = 7200
        expire = 604800
        minimum = 60
        ttl = 60
    ADDITIONAL RECORDS:
------------
** server can't find go-http-server-service.default.default.svc.cluster.local: NXDOMAIN
Server:         10.76.0.10
Address:        10.76.0.10#53
------------
    QUESTIONS:
        go-http-server-service.default.svc.cluster.local, type = A, class = IN
    ANSWERS:
    ->  go-http-server-service.default.svc.cluster.local
        internet address = 10.76.1.109
        ttl = 18
    AUTHORITY RECORDS:
    ADDITIONAL RECORDS:
------------
Non-authoritative answer:
Name:   go-http-server-service.default.svc.cluster.local
Address: 10.76.1.109
------------
    QUESTIONS:
        go-http-server-service.default.svc.cluster.local, type = AAAA, class = IN
    ANSWERS:
    AUTHORITY RECORDS:
    ->  cluster.local
        origin = ns.dns.cluster.local
        mail addr = hostmaster.cluster.local
        serial = 1613232000
        refresh = 28800
        retry = 7200
        expire = 604800
        minimum = 60
        ttl = 60
    ADDITIONAL RECORDS:
```

### 最後把東西清掉
```bash
kubectl -n "${ns}" delete -f https://gitlab.com/conan-chang/example-code/-/raw/master/simple-go-http-server/k8s-deployment.yaml
```

### 別的 namespace
```bash
kubectl -n "test-service" run -it --rm --restart=Never simple-tool --image=alpine:3.13 sh
```
裡面的 /etc/esolv.conf，按照這個設定，直接找 `go-http-server-service.default` 就不會解析到 IP 了
```shell
/ # cat /etc/resolv.conf
search test-service.svc.cluster.local svc.cluster.local cluster.local asia-east1-a.c.conanc.internal c.conanc.internal google.internal
nameserver 10.76.0.10
options ndots:5
```
