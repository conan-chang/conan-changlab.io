---
title: 利用 GitLab 建個 timmer 吧
subtitle: GitLab CI 擴展
date: 2022-02-28
categories: ["share"]
tags: ["gitlab", "tool"]
comments: true
---

## 前言
有個朋友團隊要進入全 GCP 雲端環境，目前使用 GitLab 做為 git server，部分 CI/CD 需要 Windowns runner 來執行程式碼編譯。那問題來了，編譯需要 CPU 數較高的機器，在 GCP 上開 Windows 除了要付機器錢之外，還要付 Windows server license，這如果 24 小時開著就很花浪費錢了。 

### 解法思路
中間找了一下 GitLab 不支援當 pipeline 啟動時觸發開機，runner 長時間不使用則關機的設定(比較類似的是 docker machine，但是我們要 Windows，而且 docker machine bug 太多了)。再來第二個想法就會是在原本的 pipeline 開頭加入開機(由另外 24 小時開機的 runner 操作)，結尾加上關機的 job 不就好了嗎？對，如果今天只有一個專案是可以這樣，不過如果有 20 或以上的專案的話，因為共用 runner 會造成無法預估的開關機行為。所以要為每個專案設定專屬的 runner 就要花很多時間去設定了。  
最後就想到建個 timer 不就好了，timer 代表 runner 有在被使用，開始倒數的時候就開機，倒數結束就關機。每當有新的 pipeline 需要這個 runner 的時候就重新開始倒數，這樣就可以很好的解決共用 runner 的問題了

## 實際在 GitLab 呈現
具體就是像這張圖一樣的：
1. 利用一個 container REPO 來紀錄最後被存取的時間(commit log)
   1. 被觸發的時候：寫一個 empty commit，相當於重新倒數
   2. schedules 觸發：檢查最後的 commit 距離現在多長時間，相當於檢查結束倒數了沒
2. 要使用 Windows runner 的 application REPO 就在 pipeline 前發一個 webhook 到 container REPO 來啟動 runner
![gitlab-share-runner-auto-shutdown.png](./images/gitlab-share-runner-auto-shutdown.png)

## 最後成果範例
https://gitlab.com/conan-example/clock-reset/clock-contoller  
這個範例我是沒有把開機器的 gcloud 指令也加進來，因為不想局限這個範例的可能性，它本質其實就是一個 timer，你可以拿去做不同的用途  
要執行要注意的是：
1. application REPO 要觸發另一個 REPO 的 pipeline 我是使用 webhook，所以要帶個 triggers token(settings->CI/CD->Pipeline triggers)
   1. 這裡可能會思考說為何不用內建就好，要用 webhook，這是因為希望能給全 GitLab 使用，使用 upstream/downstream pipeilne 會面對另外的權限問題
2. 而 container REPO 要寫 commit 給自己要帶個可以 write_repository 權限的 token，這裡我是用自己的 personal token。
